package id.co.asyst.amala.download.notes.utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import org.apache.camel.Exchange;
import org.apache.camel.Message;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class Utils {

    public void validation(Exchange exchange) {
        Map<String, Object> datareq = exchange.getProperty("originaldatarequest", Map.class);
        Map<String, Object> identity = exchange.getProperty("identity", Map.class);
        String responsemessage = "";
        String latian = "satu";
        Boolean valid = true;
        Boolean useperiod = false;
        Boolean usetype = false;
        Boolean uselevel = false;
        if (datareq.get("memberid") == null) {
            valid = false;
            responsemessage = "Memberid is required";
        }

        if (datareq.get("createddatestart") != null) {
            if (datareq.get("createddateend") == null) {
                valid = false;
                responsemessage = "Createddateend is required";
            }
            useperiod = true;
        }

        if (datareq.get("createddateend") != null) {
            if (datareq.get("createddatestart") == null) {
                valid = false;
                responsemessage = "Createddatestart is required";
            }
            useperiod = true;
        }

        if (datareq.get("type") != null) {
            usetype = true;
        }
        if (datareq.get("level") != null) {
            uselevel = true;
        }


        exchange.setProperty("validateReq", valid);
        exchange.setProperty("resmsg", responsemessage);

        exchange.setProperty("memberid", datareq.get("memberid").toString());
        exchange.setProperty("type", datareq.get("type"));
        exchange.setProperty("level", datareq.get("level"));
        exchange.setProperty("createddatestart", datareq.get("createddatestart"));
        exchange.setProperty("createddateend", datareq.get("createddateend"));

        exchange.setProperty("useperiod", useperiod);
        exchange.setProperty("usetype", usetype);
        exchange.setProperty("uselevel", uselevel);
    }

    public void generateResult(Exchange exchange) throws ParseException, IOException {
        Message in = exchange.getIn();
        Message out = exchange.getOut();
        Gson gson = new Gson();
        List<Map<String, Object>> result = in.getBody(List.class);
        List<Map<String, Object>> resultnew = new LinkedList<>();
        ArrayList<String> before = new ArrayList<>();
        ArrayList<String> after = new ArrayList<>();
        Map<String, Object> header = new LinkedHashMap<>();
        Map<String, Object> data = new LinkedHashMap<>();

        Map<String, Object> profile = exchange.getProperty("resultprofile", Map.class);
        List<Map<String, Object>> membercards = (List<Map<String, Object>>) profile.get("membercards");
        String cardnumber = membercards.get(0).get("cardnumber").toString();
        String firstname = profile.get("firstname").toString();
        String lastname = "";
        if (profile.get("lastname") != null) {
            lastname = profile.get("lastname").toString();
        }
        String name = firstname + " " + lastname;
        String filename = "Notes " + firstname + " " + cardnumber;

        header.put("No", "No");
        header.put("Created Date", "Created Date");
        header.put("Type", "Type");
        header.put("Level", "Level");
        header.put("Notes", "Notes");
        header.put("Data Before", "Data Before");
        header.put("Data After", "Data After");
        header.put("Cardnumber", "Cardnumber");
        header.put("Name", "Name");
        header.put("Created By", "Created By");

        SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
        int no = 0;
        for (int i = 0; i < result.size(); i++) {
            Map<String, Object> mappingbefore = new ObjectMapper().readValue((String) result.get(i).get("databefore"), HashMap.class);
            Map<String, Object> mappingafter = new ObjectMapper().readValue((String) result.get(i).get("dataafter"), HashMap.class);
            System.out.println("databefore ::: " +mappingbefore);
            System.out.println("databefore ::: " +mappingafter);
//            Map<String, Object> databefore = mappingbefore;
//            Map<String, Object> dataafter = mappingafter;
            ArrayList<String> keyListBefore = new ArrayList<String>(mappingbefore.keySet());
            System.out.println("keyListBefore ::: " +keyListBefore);
            ArrayList<Object> valueListBefore = new ArrayList<Object>(mappingbefore.values());


            ArrayList<String> keyListAfter = new ArrayList<String>(mappingafter.keySet());
            System.out.println("keyListAfter ::: " +keyListAfter);
            ArrayList<Object> valueListAfter = new ArrayList<Object>(mappingafter.values());

            for (int a = 0 ; a < keyListBefore.size(); a++){
                String initbefore = keyListBefore.get(a).toString() + " " +valueListBefore.get(a);
                String initafter = keyListAfter.get(a).toString() + " " +valueListAfter.get(a);
                before.add(initbefore);
                after.add(initafter);
            }
            String initcrdate = result.get(i).get("createdDate").toString();
            Date date = sdf2.parse(initcrdate);
            String crdate = sdf2.format(date);
            System.out.println("Add number on result ::: " + result);
            result.get(i).remove("updatedBy");
            result.get(i).remove("updatedDate");
            result.get(i).remove("membernotesid");
            result.get(i).remove("memberid");
            result.get(i).remove("createddateend");
            result.get(i).remove("createddatestart");
            result.get(i).put("Cardnumber", cardnumber);
            result.get(i).put("Name", name);
            result.get(i).put("Created By", result.get(i).get("createdBy"));
            result.get(i).replace("createdBy", i + 1);
            result.get(i).replace("createdDate", crdate);
            result.get(i).replace("databefore", before);
            result.get(i).replace("dataafter", after);
        }
//        for (int i = 0 ; i < result.size(); i++) {
//            no= new Integer(no + 1);
//            System.out.println("set number ::: " + no);
//            String initcrdate = result.get(i).get("createdDate").toString();
//            Date date = sdf2.parse(initcrdate);
//            String crdate = sdf2.format(date);
//            data.put("No", result.get(i).get("No"));
//            data.put("Cardnumber", cardnumber);
//            data.put("Name", name);
//            data.put("Type", result.get(i).get("type"));
//            data.put("Level", result.get(i).get("level"));
//            data.put("Notes", result.get(i).get("notes"));
//            data.put("Data Before", result.get(i).get("databefore"));
//            data.put("Data After", result.get(i).get("dataafter"));
//            data.put("Created By", result.get(i).get("createdBy"));
//            data.put("Created Date", crdate);
//            resultnew.add(data);
//        }
        result.add(0, header);
        System.out.println("RESULT AFTER :: " + result);
        exchange.setProperty("filename", filename);
        out.setBody(result);
    }

    public void generateResultExcel(Exchange exchange) throws ParseException {
        Message in = exchange.getIn();
        Message out = exchange.getOut();
        Gson gson = new Gson();
        List<Map<String, Object>> result = in.getBody(List.class);
        List<Map<String, Object>> resultnew = new ArrayList<>();
        Map<String, Object> header = new LinkedHashMap<>();
        Map<String, Object> data = new HashMap<>();

        Map<String, Object> profile = exchange.getProperty("resultprofile", Map.class);
        List<Map<String, Object>> membercards = (List<Map<String, Object>>) profile.get("membercards");
        String cardnumber = membercards.get(0).get("cardnumber").toString();
        String firstname = profile.get("firstname").toString();
        String lastname = profile.get("lastname").toString();
        String filename = "Notes " + firstname + " " + cardnumber;
    }
}
